from aiogram.dispatcher.filters.state import StatesGroup, State


class Wallet(StatesGroup):
    start = State()
    replenish = State()
    withdraw = State()
    transfer = State()


from util import Wallet
from aiogram import Bot, Dispatcher, executor, types
from config import API_TOKEN
from aiogram.dispatcher import FSMContext
from aiogram.contrib.fsm_storage.memory import MemoryStorage
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot,  storage=MemoryStorage())


@dp.message_handler(state=Wallet.start)
async def start():
    keyboard_markup = types.ReplyKeyboardMarkup(row_width=2)
    button_text = ("Replenish Wallet", "Withdraw Money")
    keyboard_markup.add(*(types.KeyboardButton(text) for text in button_text))
    more_buttons_text = ("Convert Money", "Convert Cryptocurrency")
    keyboard_markup.add(*(types.KeyboardButton(text) for text in more_buttons_text))



@dp.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message, state:FSMContext):
    async with state.proxy() as data:
        data['money'] = 0
    await message.reply("Hello!\nI am your A.I assistant!\nMy name is MOON!\nAnd I will help you to navigate through this telegram bot!\n Since it's your first time here, you will need to replenish your wallet, to do so click or tap on 'Replenish Wallet'.")
    await Wallet.start.set()


@dp.message_handler()
async def process_answer(message: types.Message):
    button_text = message.text
    if button_text == "Replenish Wallet":
        await Wallet.replenish.set()
        await message.answer("Enter an amount of money you want to replenish.")
    if button_text == "Withdraw Money":
        await Wallet.withdraw.set()
        await message.answer("Enter an amount of money you want to withdraw.")
    if button_text == "Transfer Money":
        await message.answer("Sorry, but due to political situation in your country, this function is not accessible.")
        await Wallet.start.set()
    if button_text == "Convert Cryptocurrency":
        await message.answer("Sorry, but this is an alpha of this project, so this function is not accessible.")
        await Wallet.start.set()

        
@dp.message_handler(state=Wallet.replenish)
async def process_replenish(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        money = data['money']
    reply = message.text
    for symbol in list(reply.split()):
        if symbol != ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]:
            print("Input is not correct! To replenish your wallet enter only numbers.")
        else:
            money += int(reply)
            data['money'] += int(reply)
            await message.answer(f"Your account has been updated. Now you have {money} in your account.")
            await Wallet.start.set()


@dp.message_handler(state=Wallet.withdraw)
async def process_replenish(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        money = data['money']
    reply = message.text
    for symbol in list(reply.split()):
        if symbol != ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]:
            print("Input is not correct! To withdraw money enter only numbers.")
        else:
            money -= int(reply)
            data['money'] -= int(reply)
            await message.answer(f"Your account has been updated. Now you have {money} in your account.")
            await Wallet.start.set()


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)